import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "Ir ao Mercado",
      date: "2024-02-27",
      time: "10:00",
      address: "Super SS",
    },
    {
      id: 2,
      title: "Estudar React Native",
      date: "2024-02-28",
      time: "14:30",
      address: "Em casa",
    },
    {
      id: 3,
      title: "Fazer exercícios físicos",
      date: "2024-03-01",
      time: "08:00",
      address: "Academia Fitness",
    },
  ];
  const taskPress = (task) => {
    navigation.navigate("DetalhesDasTarefas", {task});
  };
  return (
    <View>
      <FlatList
        data={tasks}
        keyExtractor={(item) => item.id.toString}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => taskPress(item)}>
            <Text>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
export default TaskList;
